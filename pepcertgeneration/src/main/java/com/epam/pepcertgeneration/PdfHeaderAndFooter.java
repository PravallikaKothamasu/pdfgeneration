package com.epam.pepcertgeneration;

import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;

public class PdfHeaderAndFooter {
    public static void main(String[] args) throws Exception{
        Document document = new Document();
        try {
            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream("HeaderFooter.pdf"));
            document.open();
            Image headerImage = Image.getInstance("src/main/resources/images/header.png");
            headerImage.setAbsolutePosition(0, 563);
            document.add(headerImage);
            Image footerImage = Image.getInstance("src/main/resources/images/footer.png");
            footerImage.setAbsolutePosition(0, 0);
            document.add(footerImage);
            document.close();
            pdfWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
