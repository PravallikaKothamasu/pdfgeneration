package com.epam.pepcertgeneration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PepcertgenerationApplication {

	public static void main(String[] args) {
		SpringApplication.run(PepcertgenerationApplication.class, args);
	}

}
