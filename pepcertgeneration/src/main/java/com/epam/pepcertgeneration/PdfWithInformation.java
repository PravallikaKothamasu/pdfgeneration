package com.epam.pepcertgeneration;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class PdfWithInformation {
    public static final String SRC = "HeaderFooter.pdf";
    public static final String DEST = "pepcert"+".pdf";
    public static void main(String[] args) throws IOException, DocumentException {
        File file = new File(DEST);
        new PdfWithInformation().manipulatePdf(SRC, DEST);
    }

    public void manipulatePdf(String src, String dest) throws IOException, DocumentException {
        FontFactory.register("src/main/resources/fonts/CalibriBoldItalic.ttf", "calibriBoldItalic");
        FontFactory.register("src/main/resources/fonts/CalibriRegular.ttf", "calibriRegular");
        FontFactory.register("src/main/resources/fonts/CalibriBold.TTF", "calibriBold");
        //Font calibriBoldItalicFont = FontFactory.getFont("calibriBoldItalic", BaseFont.IDENTITY_H);
        //Font calibriRegularFont = FontFactory.getFont("calibriRegular", BaseFont.IDENTITY_H);
        //Font calibriBoldAnsi = FontFactory.getFont("calibriBold", BaseFont.WINANSI);
        PdfReader reader = new PdfReader(src);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
        PdfContentByte cb = stamper.getOverContent(1);
        ColumnText columnText = new ColumnText(cb);
        columnText.setSimpleColumn(75f, 550f, 520f, 0f);
        Paragraph heading = new Paragraph();
        heading.add("Certificate of Participation");
        heading.setFont(FontFactory.getFont("calibriBoldItalic", BaseFont.IDENTITY_H, 28));
        heading.setAlignment(Element.ALIGN_CENTER);
        heading.setSpacingAfter(25);
        Paragraph presentedToParagraph = new Paragraph();
        presentedToParagraph.add("Presented to");
        Font calibriRegularFont = FontFactory.getFont("calibriRegular", BaseFont.IDENTITY_H, 15);
        calibriRegularFont.setColor(BaseColor.BLACK);
        presentedToParagraph.setFont(calibriRegularFont);
        presentedToParagraph.setAlignment(Element.ALIGN_CENTER);
        presentedToParagraph.setSpacingAfter(25);
        Paragraph name = new Paragraph();
        name.add("Pravallika Kothamasu");
        name.setFont(FontFactory.getFont("calibriBold", BaseFont.WINANSI, 23));
        name.setAlignment(Element.ALIGN_CENTER);
        name.setSpacingAfter(10);
        Paragraph paragraph = new Paragraph(25);
        Font paragraphFont = FontFactory.getFont("calibriRegular", BaseFont.IDENTITY_H, 15);
        paragraphFont.setColor(BaseColor.BLACK);
        Font paragraphBoldFont = FontFactory.getFont("calibriBold", BaseFont.WINANSI, 15);
        Font paragraphBoldItalicFont = FontFactory.getFont("calibriBoldItalic", BaseFont.WINANSI, 15);
        Chunk initialChunk = new Chunk("For successful completion of Online");
        initialChunk.setFont(paragraphFont);
        Chunk  pepChunk = new Chunk(" Pre Education Program (PEP)");
        pepChunk.setFont(paragraphBoldFont);
        Chunk conductChunk = new Chunk(" conducted by");
        conductChunk.setFont(paragraphFont);
        Chunk epamChunk = new Chunk(" EPAM Systems India Pvt Ltd");
        epamChunk.setFont(paragraphBoldFont);
        Chunk fromChunk = new Chunk(" from");
        fromChunk.setFont(paragraphFont);
        Chunk startDateChunk = new Chunk(" 26th Feb 2019");
        startDateChunk.setFont(paragraphBoldItalicFont);
        Chunk toChunk = new Chunk(" to");
        toChunk.setFont(paragraphFont);
        Chunk lastDateChunk = new Chunk(" 9th July 2019. ");
        lastDateChunk.setFont(paragraphBoldItalicFont);
        Chunk remainingContent = new Chunk(" “Software Engineering Best" +
                "Practices”, “OOPS Design Principles”, “Release Strategy” and “Introduction to front" +
                "end technology and Testing’ beginner level modules were covered as part of the " +
                "program, over 14 online webinars, with hands-on technical tasks. ");
        remainingContent.setFont(paragraphFont);
        paragraph.add(initialChunk);
        paragraph.add(pepChunk);
        paragraph.add(conductChunk);
        paragraph.add(epamChunk);
        paragraph.add(fromChunk);
        paragraph.add(startDateChunk);
        paragraph.add(toChunk);
        paragraph.add(lastDateChunk);
        paragraph.add(remainingContent);
        paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
        paragraph.setSpacingAfter(35);
        PdfPTable tableWithDateAndSignature = new PdfPTable(2);
        tableWithDateAndSignature.setWidths(new int[]{ 6, 4 });
        Phrase datePhrase = new Phrase("Date: "+"June 17, 2020");
        datePhrase.setFont(FontFactory.getFont("calibriRegular", BaseFont.IDENTITY_H, 30));
        PdfPCell dateCell = new PdfPCell(datePhrase);
        dateCell.setPaddingTop(100f);
        dateCell.setBorder(Rectangle.NO_BORDER);
        Image signatureimage = Image.getInstance("src/main/resources/images/signature.png");
        PdfPCell signatureCell = new PdfPCell(signatureimage, true);
        signatureCell.setPaddingTop(20f);
        signatureCell.setBorder(Rectangle.NO_BORDER);
        tableWithDateAndSignature.addCell(dateCell);
        tableWithDateAndSignature.addCell(signatureCell);
        tableWithDateAndSignature.setWidthPercentage(100f);
        columnText.addElement(heading);
        columnText.addElement(presentedToParagraph);
        columnText.addElement(name);
        columnText.addElement(paragraph);
        columnText.addElement(tableWithDateAndSignature);
        columnText.go();
        stamper.close();
        reader.close();
    }
}
